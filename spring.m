r = 1/12 * 0.0254; %radius of wire
n = 40; %number of spring coils
m = 100; %mass on a spring
tau = 10; %time constant of damping
g = 9.81; %acceleration
f = 1; %frequency of a force
F = m * g; %force
R = 0.005; %radius of spring
G = 80.0 * 10^9; %modulus of stiffness / Kirchoff's modulus for steel (from https://pl.wikipedia.org/wiki/Moduł_Kirchhoffa)
k = G*r^4 / (4*n * R^3); %wzór z http://www.mif.pg.gda.pl/pl/download/LabFizNowe/rozpakowane/Cwicz63_01.pdf 
omega0 = (k / m)^0.5; %wzór z https://pl.wikipedia.org/wiki/Drgania_swobodne
beta = 1/(2*tau);

%wzory ze strony agh
%https://epodreczniki.open.agh.edu.pl/tiki-index.php?page=Drgania+wymuszone+i+rezonans#eq:Drg_wym:1

%(a)
%creating array of frequencies fs and calculating amplitudes for each
%frequency
fs = linspace(1, 50, 100);
omegas = 2*pi*fs;
disp(fs);
amplitudes = zeros(size(fs));
for i = 1:size(fs, 2)
    f = fs(i);
    omega = omegas(i);
    A = g / ((omega0^2 - omega^2)^2 + (2*beta * omega)^2)^0.5; %https://epodreczniki.open.agh.edu.pl/tiki-index.php?page=Drgania+wymuszone+i+rezonans#eq:Drg_wym:1
    amplitudes(i) = A;
end
 

%(b)
%calculating amplitudes for each coil radius
Rs = (0.005:0.002:0.051);
amplitudes = zeros(size(Rs));
for i = 1:size(Rs, 2)
    R = Rs(i);
    k = G*r^4 / (4*n * R^3);
    omega0 = (k / m)^0.5;
    omega = omegas(i);
    A = g / ((omega0^2 - omega^2)^2 + (2*beta * omega)^2)^0.5; % %https://epodreczniki.open.agh.edu.pl/tiki-index.php?page=Drgania+wymuszone+i+rezonans#eq:Drg_wym:1
    amplitudes(i) = A;
    disp(A);
end


%(c)

%calculating resonance amplitudes for three different values of R (defined
%in Rs2)
Rs2 = [0.005, 0.004, 0.003];
Amplitudes = zeros(3, 50);
for i = 1:3
    R = Rs2(i);
    k = G*r^4 / (4*n * R^3);

    omega0 = (k / m)^0.5;
    disp("omega0 = "+omega0);
    for j = 1:size(fs, 2)
        f = fs(j);
        omega = omegas(j);
        A = g / ((omega0^2 - omega^2)^2 + (2*beta * omega)^2)^0.5; %https://epodreczniki.open.agh.edu.pl/tiki-index.php?page=Drgania+wymuszone+i+rezonans#eq:Drg_wym:1
        Amplitudes(i, j) = A;
    end
end

%Plotting amplitudes for three different radiuses
fig = figure;
subplot(2, 1, 1)
plot(omegas, Amplitudes(1, :), omegas, Amplitudes(2, :), omegas, Amplitudes(3, :))
title({
    ('Amplitude for different spring radiuses')
    ('R = 0.005 (blue), R = 0.004 (orange), R = 0.003 (yellow)')
    });
xlabel('omega [Hz]')
ylabel('A [m]')

%(d)
%calculating resonance amplitudes for different values of R (defined in
%Rs3)
resonanceAmplitudes = zeros(1, 50);
resonanceOmegas = zeros(1, 50);

Rs3 = linspace(0.005, 0.1, 50);

for i = 1:50
    R = Rs3(i);
    k = G*r^4 / (4*n * R^3);
    omega0 = (k / m)^0.5;
    resonanceOmegas(i) = (omega0^2 - 2*beta^2)^0.5;
    A = g / (2*beta * (omega0^2 - beta^2)^0.5);
    disp("omega0 = " + omega0 + "    R = " + R + "    A = " + A);
    resonanceAmplitudes(i) = g / (2*beta * (omega0^2 - beta^2)^0.5);
end
disp(beta);

%plotting resonance amplitudes
subplot(2, 1, 2)
plot(Rs3, resonanceAmplitudes)
title('Resonance amplitudes for different R')
xlabel('R [m]')
ylabel('Amax [m]')

%(e)
%saving plots to .pdf file
saveas(fig,'resonance.pdf')

%(f)
%creating a header for .dat file
header = ["Coil radius r", r, "[m]"
    "Coil wire radius R", R, "[m]"
    "Number of coils n", n, "[1]"
    "Mass m", m, "[kg]"
    "Force F", F, "[N]"
    "Acceleration g", g, "[m/s^2]"
    "Damping time constant tau ", tau, "[s]"
    "Damping coefficient beta", beta, "[1/s]"
    "Kirchoff's modulus for steel G", G, "[Pa]"
    "", "", ""
    "#####", "#####", "#####"
    "", "", ""
    "R [m]", "Resonance amplitude [m]", "Resonance omega [Hz]"];

%writing a header to a file
filename = "resonance_analysis_results.dat";
writematrix(header, filename);

%transposing arrays from row to column vectors
Rs3 = transpose(Rs3);
resonanceAmplitudes = transpose(resonanceAmplitudes);
resonanceOmegas = transpose(resonanceOmegas);

%concatenating arrays
data = cat(2, Rs3, resonanceAmplitudes, resonanceOmegas);

%appending concatenated arrays to .dat file
writematrix(data, filename, 'WriteMode', 'append');